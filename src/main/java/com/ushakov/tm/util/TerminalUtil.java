package com.ushakov.tm.util;

import com.ushakov.tm.exception.system.IndexIncorrectException;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value);
        }
    }

}
