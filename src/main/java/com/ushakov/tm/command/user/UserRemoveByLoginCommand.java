package com.ushakov.tm.command.user;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final User user = serviceLocator.getUserService().removeOneByLogin(login);
    }

    @Override
    @NotNull
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
