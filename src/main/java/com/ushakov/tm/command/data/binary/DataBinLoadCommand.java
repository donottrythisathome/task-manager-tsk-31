package com.ushakov.tm.command.data.binary;

import com.ushakov.tm.command.AbstractDataCommand;
import com.ushakov.tm.dto.Domain;
import com.ushakov.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBinLoadCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Load data from binary file.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @Override
    @NotNull
    public String name() {
        return "data-load-bin";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
