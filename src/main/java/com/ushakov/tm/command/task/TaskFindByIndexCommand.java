package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskFindByIndexCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Find task by index.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER TASK INDEX");
        @NotNull final Integer taskIndex = TerminalUtil.nextNumber();
        @NotNull final Task task = serviceLocator.getTaskService().findOneByIndex(userId, taskIndex - 1);
        System.out.println(task);
    }

    @Override
    @NotNull
    public String name() {
        return "find-task-by-index";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
